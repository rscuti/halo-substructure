import pyfits
import numpy as np
from pylab import *
from mpl_toolkits.mplot3d import Axes3D
from matplotlib import cm
import matplotlib.pyplot as plt
import random as rand
from astropy.table import Table

Nparticle = 5000
size_box = 50

seed = 99923234
rand.seed(seed)

x = []
y = []
z = []

for i in range(Nparticle):

#Uniform Distribution
    #x.append(rand.uniform(0, size_box))
    #y.append(rand.uniform(0, size_box))
    #z.append(rand.uniform(0, size_box))

#Gaussian Distribution
    #x.append(rand.gauss(25, size_box/6))
    #y.append(rand.gauss(25, size_box/6))
    #z.append(rand.gauss(25, size_box/6))
    
#Poisson
    x.append(np.random.poisson(3))
    y.append(np.random.poisson(3))
    z.append(np.random.poisson(3))

#print x
#print y
#print z
    

fig = plt.figure()
ax1 = fig.add_subplot(111, projection='3d')
ax1.scatter(x,y,z, c=z,cmap=cm.gist_rainbow)

ax1.set_xlabel('X')
ax1.set_ylabel('Y')
ax1.set_zlabel('Z')
ax1.set_xlim(0, 10)
ax1.set_ylim(0, 10)
ax1.set_zlim(0, 10)
#ax1.set_title('Uniform Distribution')
#ax1.set_title('Gaussian Distribution')
ax1.set_title('Poisson Distribution')

#Save Figures

#savefig("C:\\Pranav\\My Documents\\Independent Research\\2014_Research\\SubStructure\\Pictures\\Uniform.png")
#savefig("C:\\Pranav\\My Documents\\Independent Research\\2014_Research\\SubStructure\\Pictures\\Gaussian.png")
savefig("C:\\Pranav\\repos\\halo-substructure\\Pictures\\Poisson.png")

#Save Tables
dataTable = Table([x,y,z], names=('x', 'y', 'z'))
print dataTable
dataTable.write('Poisson.txt', format='ascii.tab')
#plt.show()

